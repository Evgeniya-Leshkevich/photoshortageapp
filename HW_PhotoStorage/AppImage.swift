import Foundation

class AppImage: Codable {
    var selectedImage: String?
    var commentsText: String?
    var favoriteImage: Bool?
    
    init(selectedImage: String?,commentsText: String?, favoriteImage: Bool?) {
        self.selectedImage = selectedImage
        self.commentsText = commentsText
        self.favoriteImage = favoriteImage
    }
    
    private enum CodingKeys: String, CodingKey {
        case selectedImage
        case commentsText
        case favoriteImage
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        selectedImage = try container.decodeIfPresent(String.self, forKey: .selectedImage)
        commentsText = try container.decodeIfPresent(String.self, forKey: .commentsText)
        favoriteImage = try container.decodeIfPresent(Bool.self, forKey: .favoriteImage)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.selectedImage, forKey: .selectedImage)
        try container.encode(self.commentsText, forKey: .commentsText)
        try container.encode(self.favoriteImage, forKey: .favoriteImage)
    }
    
}
