import UIKit

class CustomCollectionViewAddCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var addButtonView: UIView!
    
    // MARK: - FLow func
    func configure() {
        self.addButton.frame = CGRect(x: addButton.frame.origin.x, y: addButton.frame.origin.y, width: addButton.frame.width, height: addButton.frame.height)
        self.addButton.setBackgroundImage(UIImage(named: "icAdd"), for: .normal)
        self.addButton.contentMode = .scaleAspectFill
        self.addButtonView.addSubview(self.addButton)
    }
    
}
