import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var selectedView: UIImageView!
    
    // MARK: - VC life Func
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 15
    }
    
    override var isSelected: Bool {
        didSet {
            self.selectedView.isHidden = !isSelected
        }
    }
    
    // MARK: - FLow func
    func configure (object: AppImage) {
        if let photo = object.selectedImage {
            self.photoImageView.image = Manager.shared.loadImage(fileName: photo)
            self.photoImageView.contentMode = .scaleAspectFill
        }
    }
}



