import Foundation
import UIKit

class AppImageManager {
    
    static let shared = AppImageManager()
    private init() {}
    
    
    func saveImageData(images: AppImage, key: String) {
        var array = self.loadImageData(key: key)
        array.append(images)
        UserDefaults.standard.set(encodable: array, forKey: key)
    }
    
    func loadImageData(key: String) -> [AppImage] {
        guard let images = UserDefaults.standard.value([AppImage].self, forKey: key) else {
            return []
        }
        return images
    }
    
    func deleteItem(item: Int, key: String ) {
        var array = self.loadImageData(key: key)
        array.remove(at: item)
        UserDefaults.standard.set(encodable: array, forKey: key)
    }
}
