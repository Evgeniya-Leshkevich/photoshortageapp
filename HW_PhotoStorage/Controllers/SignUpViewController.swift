import UIKit

protocol SignUpViewControllerDelegate: AnyObject {
    func loadUserSignUpData(userName: String, userPassword: String)
}

class SignUpViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordConfirmationTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signUpView: UIView!
    
    // MARK: - var
    weak var delegate: SignUpViewControllerDelegate?
    var userCredintials = false
    
    // MARK: - VC life Func
    override func viewDidLoad() {
        super.viewDidLoad()
        self.signUpView.layer.cornerRadius = 15
        self.configureTextField()
    }
    
    // MARK: - IBAction
    @IBAction func signUpButtonPressed(_ sender: UIButton) {
        self.checkPassword()
    }
    
    // MARK: - FLow func
    private func goToPhotoPickerViewController() {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "PhotoPickerViewController") as? PhotoPickerViewController else{
            return
        }
        UserDefaults.standard.clear()
        controller.userCredintials = self.userCredintials
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func checkPassword() {
        if self.userNameTextField.text == "" || self.passwordTextField.text == "" {
            self.showAlert(title: "Sign Up Error", message: "Please enter username or password", actionTitles: ["Ok"], actionStyle: [.cancel], actions: [nil], vc: self)
        } else {
            if let userName = userNameTextField.text,
               let userPassword = passwordTextField.text {
                self.delegate?.loadUserSignUpData(userName: userName, userPassword: userPassword)
            }
            self.checkPasswordEquality()
        }
    }
    
    private func checkPasswordEquality() {
        if passwordTextField.text == passwordConfirmationTextField.text {
            self.userCredintials = true
            self.goToPhotoPickerViewController()
        } else {
            self.showAlert(title: "Password confirmation doesn't match password", message: "Please try again", actionTitles: ["Ok"], actionStyle: [.cancel], actions: [nil], vc: self)
        }
    }
    
    private func configureTextField() {
        self.passwordTextField.textContentType = .oneTimeCode
        self.passwordConfirmationTextField.textContentType = .oneTimeCode
        self.userNameTextField.textContentType = .oneTimeCode
        self.passwordTextField.isSecureTextEntry = true
        self.passwordConfirmationTextField.isSecureTextEntry = true
    }
    
}

// MARK: - extension
extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
