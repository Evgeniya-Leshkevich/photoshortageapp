import UIKit

//MARK: - Enums
enum ImageSource {
    case photoLibrary
    case camera
}

enum Mode {
    case view
    case select
}

enum Keys: String {
    case firstKey = "key"
    case secondKey = "secondKey"
}

class PhotoPickerViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var photoCollectionView: UICollectionView!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var mainMenu: UIView!
    @IBOutlet weak var selectButton: UIButton!
    
    // MARK: - let
    private let itemPerRow: CGFloat = 2
    private let sectionInserts = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    
    // MARK: - var
    var numder = 0
    var userCredintials = false
    var selectedImages: [AppImage]?
    var key = ""
    var dictionarySelectedIndexPath: [IndexPath: Bool] = [:]
    
    var selectedMode: Mode = .view {
        didSet {
            switch selectedMode {
            case .view:
                for (key, value) in dictionarySelectedIndexPath {
                    if value {
                        self.photoCollectionView.deselectItem(at: key, animated: true)
                    }
                }
                self.dictionarySelectedIndexPath.removeAll()
                self.photoCollectionView.allowsMultipleSelection = false
            case .select:
                self.photoCollectionView.allowsMultipleSelection = true
            }
        }
    }
    
    // MARK: - VC life Func
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadUserCredentials()
        self.photoCollectionView.layer.cornerRadius = 15
    }
    
    // MARK: - IBAction
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func addButtonPressed(_ sender: UIButton) {
        self.showAlertOnAddButton()
    }
    
    @IBAction func editButtonPressed(_ sender: UIButton) {
        self.editView.isHidden = false
        self.mainMenu.isHidden = true
    }
    
    @IBAction func selectButtonPressed(_ sender: UIButton) {
        self.selectedMode = selectedMode == .view ? .select : .view
    }
    
    @IBAction func deleteButtonPressed(_ sender: UIButton) {
        self.deleteItems()
        self.dictionarySelectedIndexPath.removeAll()
        self.selectButtonPressed(selectButton)
        self.editView.isHidden = true
        self.mainMenu.isHidden = false
    }
    
    
    // MARK: - FLow func
    private func showAlertOnAddButton() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.performImagePicker(sourceType: .camera)
        }
        alert.addAction(cameraAction)
        let photoLibraryAction = UIAlertAction(title: "Photo Library", style: .default) { (_) in
            self.performImagePicker(sourceType: .photoLibrary)
        }
        alert.addAction(photoLibraryAction)
        let cancelAction = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func performImagePicker(sourceType: ImageSource) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.modalPresentationStyle = .currentContext
        imagePicker.allowsEditing = false
        switch sourceType {
        case .camera:
            imagePicker.sourceType = .camera
        case .photoLibrary:
            imagePicker.sourceType = .photoLibrary
        }
        present(imagePicker, animated: true, completion: nil)
    }
    
    private func loadUserCredentials() {
        if userCredintials == true {
            self.key = Keys.firstKey.rawValue
            self.selectedImages = AppImageManager.shared.loadImageData(key: key)
        } else {
            self.key = Keys.secondKey.rawValue
            self.selectedImages = AppImageManager.shared.loadImageData(key: key)
        }
    }
    
    private func deleteItems() {
        var deleteNeededIndexPaths: [IndexPath] = []
        for (key, value) in self.dictionarySelectedIndexPath {
            if value {
                deleteNeededIndexPaths.append(key)
            }
        }
        for i in deleteNeededIndexPaths.sorted(by: { $0.item > $1.item }) {
            AppImageManager.shared.deleteItem(item: i.item - 1, key: self.key)
            self.selectedImages?.remove(at: i.item - 1)
        }
        self.photoCollectionView.deleteItems(at: deleteNeededIndexPaths)
    }
    
    private func goToMainViewContoller(indexPath: IndexPath) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else{
            return
        }
        controller.key = self.key
        controller.selectedImages = self.selectedImages
        controller.number = indexPath.row - 1
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}

// MARK: - extension
extension PhotoPickerViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            if let pickedImage = Manager.shared.saveImage(image: pickedImage) {
                let currentImage = AppImage(selectedImage: pickedImage, commentsText: nil, favoriteImage: false)
                if userCredintials == true {
                    self.key = Keys.firstKey.rawValue
                    AppImageManager.shared.saveImageData(images: currentImage, key: key)
                } else {
                    self.key = Keys.secondKey.rawValue
                    AppImageManager.shared.saveImageData(images: currentImage, key: key)
                }
                self.loadUserCredentials()
                self.photoCollectionView.reloadData()
                picker.dismiss(animated: true, completion: nil)
            }
        }
    }
}

extension PhotoPickerViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let selectedImages = self.selectedImages else {
            return 1 }
        return selectedImages.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 0 {
            guard let firstCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewAddCell", for: indexPath) as? CustomCollectionViewAddCell else {
                return UICollectionViewCell()
            }
            firstCell.configure()
            return firstCell
        } else {
            guard let mainCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath) as? CustomCollectionViewCell
            else {
                return UICollectionViewCell()
            }
            guard let selectedImages = self.selectedImages else {
                return UICollectionViewCell()
            }
            mainCell.configure(object: selectedImages[indexPath.item - 1])
            return mainCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch selectedMode {
        case .view:
            collectionView.deselectItem(at: indexPath, animated: true)
            self.goToMainViewContoller(indexPath: indexPath)
        case .select:
            self.dictionarySelectedIndexPath[indexPath] = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if selectedMode == .select {
            self.dictionarySelectedIndexPath[indexPath] = false
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingWidth = sectionInserts.left * (itemPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingWidth
        let widthPerItem = availableWidth / itemPerRow
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInserts
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInserts.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInserts.left
    }
}


