import UIKit

//MARK: - Enums
enum SwipeDirection {
    case left
    case right
}

class MainViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var heartButton: UIButton!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var enteredText: UITextField!
    @IBOutlet weak var mainView: UIView!
    
    // MARK: - let
    private let mainBackgroundView = UIImageView()
    private let leftBackgroundView = UIImageView()
    private let rightBackgroundView = UIImageView()
    private let fullScreenView = UIImageView()
    private let rightFullScreenView = UIImageView()
    private let leftFullScreenView = UIImageView()
    private let screenWidth = UIScreen.main.bounds.width
    private let screenHeight = UIScreen.main.bounds.height
    
    // MARK: - var
    var selectedImages: [AppImage]?
    var number = 0
    var duration = 0.5
    var commentsText = ""
    var key = ""
    var normalScreenSize = true
    
    // MARK: - VC life Func
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addFirstPicture()
        self.createMainBackgroundImage()
        self.addRecognaizers()
        self.registerForKeyboardNotifications()
        self.mainView.layer.cornerRadius = 15
    }
    
    // MARK: - IBAction
    @IBAction func heartButtonPressed(_ sender: UIButton) {
        self.makeLike()
    }
    
    @IBAction func leftButtonPressed(_ sender: UIButton) {
        self.getNextPhoto()
    }
    
    @IBAction func rightButtonPressed(_ sender: UIButton) {
        self.getPreviousPhoto()
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func swipeRightDetected (_recognizer: UISwipeGestureRecognizer) {
        self.doSwipe(swipeDicerction: .right)
        self.doSwipeForFullScreenView(swipeDicerction: .right)
    }
    
    @IBAction func swipeLeftDetected (_recognizer: UISwipeGestureRecognizer) {
        self.doSwipe(swipeDicerction: .left)
        self.doSwipeForFullScreenView(swipeDicerction: .left)
    }
    
    @IBAction private func keyboardWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo,
              let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
              let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            self.bottomConstraint.constant = 0
        } else {
            self.bottomConstraint.constant = keyboardScreenEndFrame.height + 10
        }
        self.view.needsUpdateConstraints()
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - FLow func
    private func createMainBackgroundImage() {
        self.mainBackgroundView.frame = self.photoImageView.frame
        self.mainBackgroundView.contentMode = .scaleToFill
        self.setImage(view: self.mainBackgroundView)
        self.mainBackgroundView.dropShadow()
        self.imageView.insertSubview(self.mainBackgroundView, belowSubview: self.leftBackgroundView)
    }
    
    private func createRightBackgroundImage() {
        self.rightBackgroundView.frame = CGRect(x: self.photoImageView.frame.origin.x + self.screenWidth, y: self.photoImageView.frame.origin.y, width: self.photoImageView.frame.width, height: self.photoImageView.frame.height)
        self.setImage(view: self.rightBackgroundView)
        self.rightBackgroundView.contentMode = .scaleToFill
        self.imageView.addSubview(self.rightBackgroundView)
    }
    
    private func makeLike() {
        guard let selectedImages = self.selectedImages else { return }
        if selectedImages[number].favoriteImage == true {
            selectedImages[number].favoriteImage = false
            self.heartButton.isSelected = false
            self.saveImagesAfteChanging()
        } else {
            selectedImages[number].favoriteImage = true
            self.heartButton.isSelected = true
            self.saveImagesAfteChanging()
        }
    }
    
    private func getNextPhoto() {
        guard let selectedImages = self.selectedImages else { return }
        self.createLeftBackgroundImage()
        self.number -= 1
        self.checkLeftTap()
        self.checkFavouriteImage()
        self.enteredText.text = selectedImages[number].commentsText
        self.animateLeftView()
    }
    
    private func getPreviousPhoto() {
        guard let selectedImages = self.selectedImages else { return }
        self.number += 1
        self.checkRightTap()
        self.createRightBackgroundImage()
        self.checkFavouriteImage()
        self.enteredText.text = selectedImages[number].commentsText
        self.animateRightView()
    }
    
    private func createLeftBackgroundImage() {
        self.leftBackgroundView.frame = self.photoImageView.frame
        self.setImage(view: self.leftBackgroundView)
        self.leftBackgroundView.contentMode = .scaleToFill
        self.imageView.addSubview(self.leftBackgroundView)
    }
    
    private func animateLeftView() {
        UIView.animate(withDuration: self.duration, delay: 0, options: .curveLinear) {
            self.createMainBackgroundImage()
            self.leftBackgroundView.frame.origin.x -= self.screenWidth
        } completion: { (_) in
            self.leftBackgroundView.removeFromSuperview()
        }
    }
    
    private func animateRightView() {
        UIView.animate(withDuration: self.duration, delay: 0, options: .curveLinear) {
            self.rightBackgroundView.frame.origin.x -= self.screenWidth
        } completion: { (_) in
            self.rightBackgroundView.removeFromSuperview()
            self.createMainBackgroundImage()
        }
    }
    
    private func setImage(view: UIImageView) {
        let imageSize = 300
        guard let selectedImages = self.selectedImages else { return }
        if let imageName = selectedImages[number].selectedImage {
            view.image = Manager.shared.loadImage(fileName: imageName)?.resized(to: CGSize(width: imageSize, height: imageSize))
        }
    }
    
    private func checkRightTap() {
        guard let selectedImages = self.selectedImages else { return }
        if self.number > selectedImages.count - 1 {
            self.number = 0
        }
    }
    
    private func checkLeftTap() {
        guard let selectedImages = self.selectedImages else { return }
        if self.number < 0 {
            self.number = selectedImages.count - 1
        }
    }
    
    private func addTaprecognizer() {
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapDetected(touch:)))
        recognizer.numberOfTapsRequired = 2
        self.view.addGestureRecognizer(recognizer)
    }
    
    @objc private func tapDetected (touch: UITapGestureRecognizer) {
        if normalScreenSize == true {
            self.createFullScreenView()
            self.normalScreenSize = false
        } else {
            self.fullScreenView.removeFromSuperview()
            self.rightFullScreenView.removeFromSuperview()
            self.leftFullScreenView.removeFromSuperview()
            self.normalScreenSize = true
        }
    }
    
    private func createFullScreenView() {
        self.fullScreenView.frame = CGRect(x: 0, y: 0, width: self.screenWidth, height: self.screenHeight)
        self.fullScreenView.contentMode = .scaleAspectFill
        self.fullScreenView.clipsToBounds = true
        self.fullScreenView.image = self.mainBackgroundView.image
        self.view.insertSubview(self.fullScreenView, belowSubview: self.leftFullScreenView)
    }
    
    private func createRightFullScreenView() {
        self.rightFullScreenView.frame = CGRect(x: self.screenWidth, y: 0, width: self.screenWidth, height: self.screenHeight)
        self.rightFullScreenView.contentMode = .scaleAspectFill
        self.rightFullScreenView.clipsToBounds = true
        self.rightFullScreenView.image = self.rightBackgroundView.image
        self.view.addSubview(self.rightFullScreenView)
    }
    
    private func createLefttFullScreenView() {
        self.leftFullScreenView.frame = CGRect(x: 0, y: 0, width: self.screenWidth, height: self.screenHeight)
        self.leftFullScreenView.contentMode = .scaleAspectFill
        self.leftFullScreenView.clipsToBounds = true
        self.leftFullScreenView.image = self.leftBackgroundView.image
        self.view.addSubview(self.leftFullScreenView)
    }
    
    private func animateLeftFullScreenView() {
        UIView.animate(withDuration: self.duration, delay: 0, options: .curveLinear) {
            self.createFullScreenView()
            self.leftFullScreenView.frame.origin.x -= self.screenWidth
        } completion: { (_) in
            self.leftFullScreenView.removeFromSuperview()
        }
    }
    
    private func animateRightFullScreenView() {
        UIView.animate(withDuration: self.duration, delay: 0, options: .curveLinear) {
            self.rightFullScreenView.frame.origin.x -= self.screenWidth
        } completion: { (_) in
            self.fullScreenView.removeFromSuperview()
        }
    }
    
    private func doSwipeForFullScreenView (swipeDicerction: SwipeDirection) {
        if normalScreenSize == false {
            switch swipeDicerction {
            case.left:
                self.createLefttFullScreenView()
                self.animateLeftFullScreenView()
            case.right:
                self.createFullScreenView()
                self.rightFullScreenView.removeFromSuperview()
                self.createRightFullScreenView()
                self.animateRightFullScreenView()
            }
        }
    }
    
    private func addRightSwiperecognizer() {
        let recognizerRight = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRightDetected(_recognizer:)))
        recognizerRight.direction = .right
        self.view.addGestureRecognizer(recognizerRight)
    }
    
    private func addLeftSwiperecognizer() {
        let recognizerLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeLeftDetected(_recognizer:)))
        recognizerLeft.direction = .left
        self.view.addGestureRecognizer(recognizerLeft)
    }
    
    private func doSwipe (swipeDicerction: SwipeDirection) {
        switch swipeDicerction {
        case.left:
            guard let selectedImages = self.selectedImages else { return }
            self.createLeftBackgroundImage()
            self.number -= 1
            self.checkLeftTap()
            self.checkFavouriteImage()
            self.enteredText.text = selectedImages[number].commentsText
            self.animateLeftView()
        case.right:
            guard let selectedImages = self.selectedImages else { return }
            self.number += 1
            self.checkRightTap()
            self.createRightBackgroundImage()
            self.checkFavouriteImage()
            self.enteredText.text = selectedImages[number].commentsText
            self.animateRightView()
        }
    }
    
    private func addFirstPicture() {
        self.setImage(view: self.mainBackgroundView)
        let comment = selectedImages?[number].commentsText
        self.enteredText.text = comment
        self.checkFavouriteImage()
    }
    
    private func checkFavouriteImage() {
        guard let selectedImages = self.selectedImages else { return }
        if selectedImages[self.number].favoriteImage == true {
            self.heartButton.isSelected = true
        } else {
            self.heartButton.isSelected = false
        }
    }
    
    private func saveImagesAfteChanging() {
        UserDefaults.standard.set(encodable: self.selectedImages, forKey: key)
    }
    
    private func saveComment() {
        guard let selectedImages = self.selectedImages else { return }
        selectedImages[self.number].commentsText = self.enteredText.text
        UserDefaults.standard.set(encodable: self.selectedImages, forKey: key)
    }
    
    private func addRecognaizers() {
        self.addTaprecognizer()
        self.addLeftSwiperecognizer()
        self.addRightSwiperecognizer()
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
}

// MARK: - extension
extension MainViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.saveComment()
        textField.resignFirstResponder()
        return true
    }
}












