import UIKit
import SwiftyKeychainKit

class SignInViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signInView: UIView!
    
    // MARK: - let
    private let keychain = Keychain(service: "Evgeniya-Leshkevich.HW-PhotoStorage")
    private let userTokenKey = KeychainKey<String>(key: "userTokenKey")
    private let passwordTokenKey = KeychainKey<String>(key: "passwordTokenKey")
    
    // MARK: - var
    var userName = ""
    var userPassword = ""
    var userCredintials = false
    
    // MARK: - VC life Func
    override func viewDidLoad() {
        super.viewDidLoad()
        self.passwordTextField.isSecureTextEntry = true
        self.signInView.layer.cornerRadius = 15
    }
    
    // MARK: - IBAction
    @IBAction func signInButtonPressed(_ sender: UIButton) {
        self.checkTextFieldAvailability()
        self.checkPassword()
        self.goToPhotoPickerViewController()
    }
    
    @IBAction func signUpButtonPressed(_ sender: UIButton) {
        self.goToSignUpViewController()
    }
    
    // MARK: - FLow func
    private func goToPhotoPickerViewController() {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "PhotoPickerViewController") as? PhotoPickerViewController else{
            return
        }
        controller.userCredintials = self.userCredintials
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func goToSignUpViewController() {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController else{
            return
        }
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func checkPassword() {
        self.loadUserInfo()
        if let password = passwordTextField.text,
           let name = usernameTextField.text {
            if password == self.userPassword && name == self.userName {
                self.userCredintials = true
            } else {
                self.userCredintials = false
            }
        }
        self.passwordTextField.text = ""
        self.usernameTextField.text = ""
    }
    
    private func checkTextFieldAvailability() {
        if let password = passwordTextField.text,
           let name = usernameTextField.text {
            if password == "" || name == "" {
                self.showAlert(title: "Password or username is not filled", message: "Please fill in the fields", actionTitles: ["Ok"], actionStyle: [.cancel], actions: [nil], vc: self)
            }
        }
    }
    
    private func saveUserInfo() {
        do {
            try keychain.set(self.userName, for : userTokenKey)
            try keychain.set(self.userPassword, for : passwordTokenKey)
        } catch let error {
            debugPrint(error)
        }
    }
    
    private func loadUserInfo() {
        do {
            if let userNameValue  = try keychain.get(userTokenKey),
               let passwordValue  = try keychain.get(passwordTokenKey) {
                self.userName = userNameValue
                self.userPassword = passwordValue
            }
        } catch let error {
            debugPrint(error)
        }
    }
    
}

// MARK: - extension
extension SignInViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension SignInViewController: SignUpViewControllerDelegate {
    func loadUserSignUpData(userName: String, userPassword: String) {
        self.userName = userName
        self.userPassword = userPassword
        self.saveUserInfo()
    }
}


